import { withAuth } from "next-auth/middleware";

export default withAuth({
  callbacks: {
    authorized: ({ req }) => {
      const sessionToken = req.cookies.get("next-auth.session-token")
        ? req.cookies.get("next-auth.session-token")
        : req.cookies.get("__Secure-next-auth.session-token");

      if (sessionToken) return true;
      else return false;
    },
  },
});

export const config = {
  matcher: [
    "/fixtures",
    "/admin/",
    "/admin/scores",
    "/outright-predictions",
    "/fixture/:id*",
    "/selections/:id*",
    "/admin/:path*",
  ],
};

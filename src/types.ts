import { type UserPrediction, type Fixture } from "@prisma/client";

export type FixtureWithPrediction = {
  fixture: Fixture;
  userHasPrediction?: boolean;
};

export type GroupedFixturesProps = {
  groupedFixtures: [string, FixtureWithPrediction[]][];
};

export type UserPredictionAuditDto = {
  prediction: UserPrediction;
  fixture: Fixture;
  predictionResult: string;
  matchResult: string;
  points?: number;
};

import FlagComponent from "~/app/components/FlagComponent";
import PieChart from "~/app/components/PieChartComponent";
import { getServerAuthSession } from "~/server/auth";
import { db } from "~/server/db";
import cupLogo from "./cup.png";
import Image from "next/image";

export default async function SelectionsOverview({
  params,
}: {
  params: { id: string };
}) {
  const userPredictionAudits = await db.userPredictionAudit.findMany({
    where: {
      userId: params.id,
    },
    include: {
      fixture: true,
      userPrediction: true,
    },
    orderBy: {
      fixture: {
        date: "desc",
      },
    },
  });

  const totalUserPoints = await db.userPoints.findUnique({
    where: {
      userId: params.id,
    },
  });
  const noCompleteFixtures = await getNoCompleteFixtures();
  const winningUserPredictions = await getWinningUserPredictions(params.id);

  const winningPercentage =
    noCompleteFixtures > 0
      ? (winningUserPredictions.length / noCompleteFixtures) * 100
      : 100;
  const losingPercentage = 100 - winningPercentage;

  const session = await getServerAuthSession();

  const getBanner = (points: number) => {
    const { bannerClasses, bannerText } = getPointStyle(points);

    return (
      <span
        className={`mb-8 me-2 rounded border px-2.5 py-0.5 text-center text-xs font-medium ${bannerClasses}`}
      >
        {bannerText}
      </span>
    );
  };

  const getPointStyle = (points: number) => {
    switch (points) {
      case 3:
        return {
          bannerClasses: "border-green-400 bg-green-100 text-green-800",
          bannerText: "CORRECT SCORE",
        };
      case 1:
        return {
          bannerClasses: "border-orange-400 bg-orange-100 text-orange-800",
          bannerText: "CORRECT TEAM",
        };
      case 0:
        return {
          bannerClasses: "border-red-400 bg-red-100 text-red-800",
          bannerText: "HARD LINES",
        };
      default:
        return { bannerClasses: "", bannerText: "" };
    }
  };

  return (
    <>
      <div className="mt-4 flex items-center justify-between">
        <h2 className="ml-16 flex-grow text-center text-xl leading-6 text-white">
          {session?.user.name}
        </h2>
        <a href="/outright-predictions">
          <Image
            src={cupLogo}
            alt="outrights"
            width={40}
            height={40}
            className="ml-8"
          />
        </a>
      </div>

      <PieChart
        points={totalUserPoints?.points ?? 0}
        winPercentage={parseFloat(winningPercentage.toFixed(0))}
        lossPercentage={parseFloat(losingPercentage.toFixed(0))}
      ></PieChart>
      <div className="flex w-full justify-center">
        <hr className="w-7/12 border-t border-gray-400" />
      </div>
      <h2 className="mb-3 mt-5 text-3xl">Results</h2>
      {userPredictionAudits.length > 0 ? (
        userPredictionAudits.map((userPrediction, index) => (
          <div key={index} className="max-w-sm p-4">
            <div
              className="flex h-full flex-col rounded-lg bg-gradient-to-b from-[#0d1e62a5] from-5% via-[#0d1e62a5] via-40% to-[#0d1e62a5] to-80% p-8 text-center"
              style={{ width: "21rem" }}
            >
              {getBanner(userPrediction.pointsRecieved)}
              <div className="mb-6 flex justify-center gap-7">
                <FlagComponent
                  country={userPrediction.fixture.homeTeam}
                  size="small"
                />
                <FlagComponent
                  country={userPrediction.fixture.awayTeam}
                  size="small"
                />
              </div>
              <div className="text mb-3 gap-3 font-bold">
                Winner: {userPrediction.winningTeam} (
                {userPrediction.fixture.homeTeamResult}-
                {userPrediction.fixture.awayTeamResult})
              </div>
              <div className="mt-5 flex gap-20">
                <div className="mb-3">
                  Selection: {userPrediction.userPrediction.homeTeamPrediction}{" "}
                  - {userPrediction.userPrediction.awayTeamPrediction}
                </div>
                <div className="mb-3 flex gap-3">
                  <div>Points: </div>
                  <div
                    className={`flex h-5 w-5 items-center justify-center rounded-full border ${getPointStyle(userPrediction.pointsRecieved).bannerClasses}`}
                  >
                    {userPrediction.pointsRecieved}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
      ) : (
        <div className="p-4 text-center text-sm">
          No user predictions available
        </div>
      )}
    </>
  );
}

const getNoCompleteFixtures = async () => {
  return await db.fixture.count({
    where: {
      isComplete: true,
    },
  });
};

const getWinningUserPredictions = async (userId: string) => {
  return await db.userPredictionAudit.findMany({
    where: {
      userId: userId,
      pointsRecieved: {
        gte: 1,
      },
    },
  });
};

import Leaderboard from "./leaderboard/page";
import "/node_modules/flag-icons/css/flag-icons.min.css";

export default async function HomePage() {
  return (
    <div className="container flex flex-col items-center justify-center gap-12 px-4 py-16 ">
      <Leaderboard></Leaderboard>
    </div>
  );
}

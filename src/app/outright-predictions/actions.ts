"use server";

import { redirect } from "next/navigation";
import { getServerAuthSession } from "~/server/auth";
import { db } from "~/server/db";

export default async function userOutrightpredictions(formData: FormData) {
  const predictedWinnerInput = formData.get("winner") as string;
  const predictedSecondPlaceInput = formData.get("second-place") as string;
  const predictedGoldenBootInput = formData.get("golden-boot") as string;

  const session = await getServerAuthSession();

  await db.userPoints.update({
    where: {
      userId: session?.user.id,
    },
    data: {
      predictedWinner: predictedWinnerInput,
      predictedSecondPlace: predictedSecondPlaceInput,
      predictedGoldenboot: predictedGoldenBootInput,
    },
  });

  redirect("/");
}

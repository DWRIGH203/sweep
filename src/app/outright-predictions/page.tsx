import { db } from "~/server/db";
import SubmitButton from "../components/SubmitButton";
import goldTrophy from "./winner.png";
import runnerUp from "./runner-up.png";
import goldenBoot from "./golden-boot.png";
import Image from "next/image";
import userOutrightpredictions from "./actions";
import { getServerAuthSession } from "~/server/auth";

export default async function OutrightPredictions() {
  const teams = await db.team.findMany({
    orderBy: {
      name: "asc",
    },
  });

  const session = await getServerAuthSession();

  const userPoints = await db.userPoints.findUnique({
    where: {
      userId: session?.user.id,
    },
  });

  const userHasOutrightPrediction =
    userPoints?.predictedWinner &&
    userPoints?.predictedSecondPlace &&
    userPoints?.predictedGoldenboot
      ? true
      : false;

  return (
    <form
      className="mx-auto flex max-w-sm flex-col items-center"
      action={userOutrightpredictions}
    >
      <h1 className="mb-8 mt-6 block text-center text-4xl font-extrabold text-white">
        Outrights
      </h1>
      <Image
        className="mb-7 mt-3"
        src={goldTrophy}
        alt="Trophy"
        width={50}
        height={50}
      />
      <select
        id="winner"
        name="winner"
        defaultValue={
          userPoints?.predictedWinner ? userPoints.predictedWinner : ""
        }
        className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-center text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500"
      >
        <option className="text-gray-400" value="" disabled>
          Select Winner
        </option>
        {teams.map((team) => (
          <option key={team.id} value={team.name}>
            {team.name}
          </option>
        ))}
      </select>
      <Image
        className="mb-7 mt-14"
        src={runnerUp}
        alt="Trophy"
        width={50}
        height={50}
      />
      <select
        id="second-place"
        name="second-place"
        defaultValue={
          userPoints?.predictedSecondPlace
            ? userPoints.predictedSecondPlace
            : ""
        }
        className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-center text-sm text-gray-900"
      >
        <option value="" disabled>
          Select Second Place
        </option>
        {teams.map((team) => (
          <option key={team.id} value={team.name}>
            {team.name}
          </option>
        ))}
      </select>
      <Image
        className="mb-7 mt-16"
        src={goldenBoot}
        alt="Trophy"
        width={50}
        height={50}
      />
      <input
        type="text"
        id="golden-boot"
        name="golden-boot"
        placeholder="Type selection"
        defaultValue={
          userPoints?.predictedGoldenboot ? userPoints.predictedGoldenboot : ""
        }
        className="mb-12 block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-center text-sm text-gray-900 placeholder-gray-400 focus:border-blue-500 focus:ring-blue-500"
      />
      <SubmitButton
        isDisabled={userHasOutrightPrediction}
        successMessage="Successfully submitted outright predictions"
      />
    </form>
  );
}

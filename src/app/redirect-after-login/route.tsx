import { type NextRequest, NextResponse } from "next/server";
import { getServerAuthSession } from "~/server/auth";

export async function GET(request: NextRequest) {
  console.log("GET /redirect-after-login");
  const session = await getServerAuthSession();
  if (session) {
    return NextResponse.redirect(new URL("/fixtures", request.url));
  }
  return NextResponse.redirect(new URL("/", request.url));
}

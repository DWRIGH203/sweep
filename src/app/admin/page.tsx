import Link from "next/link";
import { validateAdminRole } from "../utils/utils";

export default async function AdminPage() {
  await validateAdminRole();

  return (
    <div className="mt-10 flex flex-col justify-center gap-5">
      <div className="flex-container rounded-lg border-2 border-slate-200 bg-gradient-to-b from-[#0d1e62a5] from-5% via-[#0d1e62a5] via-40% to-[#0d1e62a5] to-80% p-8 font-sans">
        <Link href={"/admin/scores"}>Submit Scores</Link>
      </div>
      <div className="flex-container rounded-lg border-2 border-slate-200 bg-gradient-to-b from-[#0d1e62a5] from-5% via-[#0d1e62a5] via-40% to-[#0d1e62a5] to-80% p-8 font-sans">
        <Link href={"/prediction-summary"}>Prediction Summary</Link>
      </div>
    </div>
  );
}

import { groupFixturesByDate } from "./actions";
import GroupedScores from "./GroupedScores";

export default async function Scores() {
  const groupedFixtures = await groupFixturesByDate();

  return (
    <div>
      <GroupedScores
        props={{
          groupedFixtures: groupedFixtures,
        }}
      ></GroupedScores>
    </div>
  );
}

"use client";

import { useState } from "react";
import SubmitScore from "./SubmitScore";
import { type GroupedFixturesProps } from "~/types";

export default function GroupedScores({
  props,
}: {
  props: GroupedFixturesProps;
}) {
  const [currentIndex, setCurrentIndex] = useState(0);

  const handlePrevDate = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex > 0 ? prevIndex - 1 : props.groupedFixtures.length - 1,
    );
  };

  const handleNextDate = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex < props.groupedFixtures.length - 1 ? prevIndex + 1 : 0,
    );
  };

  const currentFixtures = props.groupedFixtures?.[currentIndex]?.[1];
  const fixtureDate = props.groupedFixtures?.[currentIndex]?.[0];

  return (
    <div>
      <h1 className="mt-8 flex justify-center rounded-lg border-0 border-slate-200 bg-gradient-to-b from-[#0d1e62a5] from-5% via-[#0d1e62a5] via-40% to-[#0d1e62a5] to-80% font-sans text-3xl">
        Final Results
      </h1>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          margin: "20px 0",
        }}
      >
        <button onClick={handlePrevDate}>&lt;</button>
        <p style={{ margin: "0 20px", fontWeight: "bold" }}>{fixtureDate}</p>
        <button onClick={handleNextDate}>&gt;</button>
      </div>

      <div className="font-sans">
        {currentFixtures?.map((fixture, index) => (
          <SubmitScore key={index} fixture={fixture.fixture}></SubmitScore>
        ))}
      </div>
    </div>
  );
}

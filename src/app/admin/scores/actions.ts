"use server";

import { type UserPrediction, type Fixture, Prisma } from "@prisma/client";
import { redirect } from "next/navigation";
import { db } from "~/server/db";
import {
  type UserPredictionAuditDto,
  type FixtureWithPrediction,
} from "~/types";

export default async function updateScores(
  fixture: Fixture,
  formData: FormData,
) {
  try {
    const homeTeamScore = Number(formData.get("homeTeamScore"));
    const awayTeamScore = Number(formData.get("awayTeamScore"));

    const matchResult = evaluateResult(homeTeamScore, awayTeamScore);

    void updateFixtureScore(homeTeamScore, awayTeamScore, fixture);

    const userPredictions = await db.userPrediction.findMany({
      where: {
        fixtureId: fixture.id,
      },
    });

    await Promise.all(
      userPredictions.map(async (prediction: UserPrediction) => {
        const predictionResult = evaluateResult(
          prediction.homeTeamPrediction,
          prediction.awayTeamPrediction,
        );

        const auditDto: UserPredictionAuditDto = {
          prediction,
          fixture,
          predictionResult,
          matchResult,
        };

        let pointsToAdd = 0;
        if (
          prediction.homeTeamPrediction === homeTeamScore &&
          prediction.awayTeamPrediction === awayTeamScore
        ) {
          pointsToAdd = 3;
        } else if (matchResult === predictionResult) {
          pointsToAdd = 1;
        }

        try {
          await addUserPredictionAudit({ ...auditDto, points: pointsToAdd });
          await addUserPoints(pointsToAdd, prediction.userId);
        } catch (error) {
          if (error instanceof Prisma.PrismaClientKnownRequestError) {
            if (error.code === "P2002") {
              // P2002 is the error code for a unique constraint violation
              console.error(
                "Scores have already been submitted for this fixture! preventing point awards...",
              );
            } else {
              console.error(
                "An error occurred awarding points:",
                error.message,
              );
            }
          }
        }
      }),
    );
  } catch (error) {
    console.error("There was a problem updating scores: ", error);
    redirect("/error");
  }

  redirect("/admin/scores");
}

const addUserPredictionAudit = async (
  userPredictionAuditDto: UserPredictionAuditDto,
) => {
  const matchResultMapping: Record<string, string> = {
    DRAW: "DRAW",
    HOME: userPredictionAuditDto.fixture.homeTeam,
    AWAY: userPredictionAuditDto.fixture.awayTeam,
  };

  const winningTeam =
    matchResultMapping[userPredictionAuditDto.matchResult] ?? "UNKNOWN";

  await db.userPredictionAudit.create({
    data: {
      userId: userPredictionAuditDto.prediction.userId,
      fixtureId: userPredictionAuditDto.fixture.id,
      userPredictionId: userPredictionAuditDto.prediction.id,
      result: userPredictionAuditDto.matchResult,
      winningTeam: winningTeam,
      pointsRecieved: userPredictionAuditDto?.points ?? 0,
    },
  });
};

const addUserPoints = async (points: number, userId: string) => {
  const streak = points > 0 ? { increment: 1 } : 0;

  await db.userPoints.update({
    where: {
      userId: userId,
    },
    data: {
      points: {
        increment: points,
      },
      streak,
    },
  });

  console.log(`Added ${points} points to user: ${userId}`);
};

const updateFixtureScore = async (
  homeTeamScore: number,
  awayTeamScore: number,
  fixture: Fixture,
) => {
  await db.fixture.update({
    where: {
      id: fixture.id,
    },
    data: {
      id: fixture.id,
      homeTeam: fixture.homeTeam,
      awayTeam: fixture.awayTeam,
      date: fixture.date,
      time: fixture.time,
      name: fixture.name,
      homeTeamResult: homeTeamScore,
      awayTeamResult: awayTeamScore,
      isComplete: true,
    },
  });
};

const evaluateResult = (homeTeamScore: number, awayTeamScore: number) => {
  return homeTeamScore == awayTeamScore
    ? "DRAW"
    : homeTeamScore > awayTeamScore
      ? "HOME"
      : "AWAY";
};

export const groupFixturesByDate = async () => {
  const firstFixtureDate = "2024-06-14T20:00:00.000Z";
  const currentDateTime = new Date(firstFixtureDate);

  const currentDate = new Date(
    currentDateTime.getFullYear(),
    currentDateTime.getMonth(),
    currentDateTime.getDate(),
  );

  const fixtures = await db.fixture.findMany({
    where: {
      date: {
        gte: currentDate,
        // lt: new Date(currentDate.getTime() + 24 * 60 * 60 * 1000),
      },
    },
    orderBy: {
      date: "asc",
    },
  });

  const grouped: Record<string, FixtureWithPrediction[]> = {};

  for (const fixture of fixtures) {
    const dateKey = new Date(fixture.date).toDateString();
    if (!grouped[dateKey]) {
      grouped[dateKey] = [];
    }

    grouped[dateKey]?.push({
      fixture: fixture,
    });
  }

  return Object.entries(grouped);
};

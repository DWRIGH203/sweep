import FlagComponent from "~/app/components/FlagComponent";
import ScoreInput from "~/app/components/ScoreInput";
import { type Fixture } from "@prisma/client";
import SubmitButton from "~/app/components/SubmitButton";
import updateScores from "./actions";

export default function SubmitScore({ fixture }: { fixture: Fixture }) {
  const updateWithFixture = updateScores.bind(null, fixture);

  return (
    <form key={fixture.id} action={updateWithFixture}>
      <div
        className={`flex w-full flex-col gap-2 space-y-4 rounded-tl-lg rounded-tr-lg text-center font-sans text-lg ${fixture.isComplete ? "border-0 border-b-0 border-green-600" : "border-0 border-b-0 border-white"} bg-gradient-to-b from-[#0d1e62a5] from-5% via-[#0d1e62a5] via-40% to-[#0d1e62a5] to-80% pt-4`}
      >
        {fixture.name} : {fixture.time}
      </div>
      <div
        className={`flex w-full flex-col gap-1 space-y-0 rounded-bl-lg rounded-br-lg ${fixture.isComplete ? "border-0 border-t-0 border-green-600" : "border-0 border-t-0 border-white"} bg-gradient-to-b from-[#0d1e62a5] from-5% via-[#0d1e62a5] via-40% to-[#0d1e62a5] to-80%`}
      >
        <div className="ml-14 mt-4 flex gap-4 p-5">
          <FlagComponent country={fixture.homeTeam} size="small" />
          <div className=" p-2">{fixture.homeTeam.toUpperCase()}</div>
        </div>
        <div className="flex p-5">
          <ScoreInput
            inputName="homeTeamScore"
            defaultValue={
              fixture.homeTeamResult ? Number(fixture.homeTeamResult) : 0
            }
          />
        </div>
        <div className="ml-14 flex gap-4 p-5">
          <FlagComponent country={fixture.awayTeam} size="small" />
          <div className="pt-2">{fixture.awayTeam.toUpperCase()}</div>
        </div>
        <div className="flex p-5">
          <ScoreInput
            inputName="awayTeamScore"
            defaultValue={
              fixture.awayTeamResult ? Number(fixture.awayTeamResult) : 0
            }
          />
        </div>
      </div>
      <div className="flex flex-wrap justify-around gap-2">
        <div className="mb-5 mt-0 pb-12 pt-5">
          <SubmitButton isDisabled={fixture.isComplete ? true : false} />
        </div>
      </div>
    </form>
  );
}

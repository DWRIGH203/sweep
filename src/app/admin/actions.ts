"use server";

import { db } from "~/server/db";

export default async function addfixture(
  prevState: {
    message: string;
  },
  formData: FormData,
) {
  await db.fixture.create({
    data: {
      name: "Fixture",
      homeTeam: formData.get("homeTeam") as string,
      awayTeam: formData.get("awayTeam") as string,
      date: formData.get("date") as string,
      time: formData.get("time") as string,
      homeTeamResult: 0,
      awayTeamResult: 0,
    },
  });
  console.log(formData);
  return { message: `Added Selections` };
}

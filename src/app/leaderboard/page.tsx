import { db } from "~/server/db";
import streakLogo from "./fire.png";
import goatLogo from "./goat.png";
import Image from "next/image";

export default async function Leaderboard() {
  const data = await db.userPoints.findMany({
    include: {
      user: true,
    },
    orderBy: {
      points: "desc",
    },
  });

  return (
    <div className="relative overflow-x-auto rounded-lg shadow-2xl shadow-slate-700">
      <table className="w-full text-left text-sm text-gray-500 drop-shadow-2xl rtl:text-right">
        <thead className="text-xs uppercase text-gray-700">
          <tr>
            <th
              scope="col"
              className="border-r border-gray-300 bg-slate-700 px-6 py-3 text-gray-200"
            >
              Rank
            </th>
            <th
              scope="col"
              className="border-r border-gray-300 bg-slate-700 px-6 py-3 text-gray-200"
            >
              User
            </th>
            <th scope="col" className="bg-slate-700 px-6 py-3 text-gray-200">
              Points
            </th>
          </tr>
        </thead>
        <tbody>
          {data.map((data, index) => (
            <tr key={data.id} className="bg-white">
              <th
                scope="row"
                className="text-gray-90 flex gap-4 whitespace-nowrap border-b border-gray-300 bg-gray-50 px-6 py-4 font-medium"
              >
                {index + 1}
                {data.streak > 2 && (
                  <Image
                    className=""
                    src={data.streak > 3 ? goatLogo : streakLogo}
                    width={20}
                    height={20}
                    alt="streak"
                    style={{ position: "relative", top: "-2px" }}
                  ></Image>
                )}
              </th>
              <td className="whitespace-nowrap border-b border-gray-300 bg-gray-50 px-6 py-4 font-medium text-gray-900">
                {data.user.name}
              </td>
              <td className="whitespace-nowrap border-b border-gray-300 bg-gray-50 px-6 py-4 font-medium text-gray-900">
                {data.points}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

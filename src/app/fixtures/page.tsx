import { type Fixture } from "@prisma/client";
import { db } from "~/server/db";
import GroupedFixtures from "../components/GroupFixtures";
import { getServerAuthSession } from "~/server/auth";
import { type FixtureWithPrediction } from "~/types";
import SuccessBanner from "../components/SuccessBanner";

export default async function Fixtures() {
  const currentDateISOString = new Date().toISOString();

  const fixtures = await db.fixture.findMany({
    where: {
      date: {
        gte: currentDateISOString,
      },
    },
    orderBy: {
      date: "asc",
    },
  });

  const groupedfixtures = await groupFixturesByDate(fixtures);

  return (
    <>
      <SuccessBanner></SuccessBanner>
      <GroupedFixtures groupedFixtures={groupedfixtures} />
    </>
  );
}

const groupFixturesByDate = async (fixtures: Fixture[]) => {
  const grouped: Record<string, FixtureWithPrediction[]> = {};

  for (const fixture of fixtures) {
    const dateKey = new Date(fixture.date).toDateString();
    if (!grouped[dateKey]) {
      grouped[dateKey] = [];
    }

    const userHasPrediction = await checkUserPrection(fixture);

    grouped[dateKey]?.push({
      fixture: fixture,
      userHasPrediction: userHasPrediction,
    });
  }

  return Object.entries(grouped);
};

const checkUserPrection = async (fixture: Fixture) => {
  const session = await getServerAuthSession();

  const userPrediction = await db.userPrediction.findFirst({
    where: {
      fixtureId: fixture.id,
      userId: session?.user.id,
    },
  });

  return userPrediction ? true : false;
};

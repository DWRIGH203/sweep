import { type Session } from "next-auth";
import { redirect } from "next/navigation";
import { getServerAuthSession } from "~/server/auth";
import { db } from "~/server/db";

export async function validateAdminRole() {
  const session = await getServerAuthSession();
  const user = await db.user.findUnique({
    where: {
      id: session?.user.id,
    },
  });

  if (user?.roleName !== "ADMIN") {
    redirect("/error");
  }
}

export async function isAdmin(session: Session | null) {
  if (session) {
    const user = await db.user.findUnique({
      where: {
        id: session?.user.id,
      },
    });

    if (user?.roleName === "ADMIN") {
      return true;
    }
  }

  return false;
}

import { db } from "~/server/db";
import moment from "moment-timezone";
import FlagComponent from "../components/FlagComponent";
import hourglass from "./hourglass.png";
import Image from "next/image";

export default async function PredictionSummary() {
  const londonNow = moment.tz("Europe/London");
  const startOfToday = londonNow.clone().startOf("day").toDate();
  const endOfToday = londonNow.clone().endOf("day").toDate();
  const currentTime = londonNow.toDate();

  const data = await db.userPrediction.findMany({
    where: {
      fixture: {
        date: {
          gte: startOfToday,
          lte: endOfToday,
        },
      },
    },
    orderBy: {
      date: "asc",
    },
    include: {
      user: true,
      fixture: true,
    },
  });

  const filteredFixtures = data.filter((prediction) => {
    const fixtureDateLondon = moment.tz(
      prediction.fixture.date,
      "Europe/London",
    );
    return fixtureDateLondon.isBefore(currentTime);
  });

  // Define the type for the accumulator
  type GroupedFixtures = Record<number, typeof filteredFixtures>;

  // Group fixtures by fixtureId
  const groupedFixtures = filteredFixtures.reduce<GroupedFixtures>(
    (acc, prediction) => {
      const fixtureId = prediction.fixture.id;
      if (!acc[fixtureId]) {
        acc[fixtureId] = [];
      }
      acc[fixtureId]?.push(prediction);
      return acc;
    },
    {},
  );

  return (
    <div className="mt-5">
      {Object.keys(groupedFixtures).length === 0 ? (
        <div className="mt-5 flex flex-col items-center gap-5">
          <h2 className="text-center text-xl text-white">
            No predictions available yet
          </h2>
          <Image src={hourglass} alt="coming soon..." width={50} height={50} />
        </div>
      ) : (
        Object.keys(groupedFixtures).map((fixtureId) => {
          const fixtureGroup = groupedFixtures[parseInt(fixtureId)]; // Added line

          if (!fixtureGroup || fixtureGroup.length === 0) {
            return <h1 key={fixtureId}>No predictions</h1>;
          }

          const homeTeam = fixtureGroup[0]?.fixture.homeTeam;
          const awayTeam = fixtureGroup[0]?.fixture.awayTeam;

          return (
            <div key={fixtureId} className="mb-6">
              <div className="relative mt-12 overflow-x-auto rounded-lg shadow-2xl shadow-slate-700">
                <table className="w-full rounded-lg text-left text-sm text-gray-500 drop-shadow-2xl rtl:text-right">
                  <thead className="text-xs uppercase text-gray-700">
                    <tr>
                      <th
                        scope="col"
                        className="border-r border-gray-300 bg-slate-700 px-6 py-3 text-gray-200"
                      >
                        Name
                      </th>
                      <th
                        scope="col"
                        className="border-r border-gray-300 bg-slate-700 px-6 py-3 text-gray-200"
                      >
                        <FlagComponent
                          country={homeTeam ? homeTeam : ""}
                          size="small"
                        />
                      </th>
                      <th
                        scope="col"
                        className="bg-slate-700 px-6 py-3 text-gray-200"
                      >
                        <FlagComponent
                          country={awayTeam ? awayTeam : ""}
                          size="small"
                        />
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {fixtureGroup.map((prediction) => (
                      <tr key={prediction.id} className="bg-white">
                        <th
                          scope="row"
                          className="whitespace-nowrap border-b border-gray-300 bg-gray-50 px-6 py-4 font-medium text-gray-900"
                        >
                          {prediction.user.name}
                        </th>
                        <td className="whitespace-nowrap border-b border-gray-300 bg-gray-50 px-6 py-4 text-center font-medium text-gray-900">
                          {prediction.homeTeamPrediction}
                        </td>
                        <td className="whitespace-nowrap border-b border-gray-300 bg-gray-50 px-6 py-4 text-center font-medium text-gray-900">
                          {prediction.awayTeamPrediction}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          );
        })
      )}
    </div>
  );
}

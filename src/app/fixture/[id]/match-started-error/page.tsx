import Link from "next/link";
import whistle from "./whistle.png";
import Image from "next/image";

export default async function MatchStartedError() {
  return (
    <div className="mt-10 flex flex-col items-center justify-center gap-3">
      <Image src={whistle} alt="Whistle" width={50} height={50} />
      <h1 className="mt-6 font-bold">Unlucky... </h1>
      <p className="mt-4 text-center">
        This match has already started and you will not be awarded any points!
      </p>
      <Link
        className="mt-8 rounded border border-white px-4 py-2 text-white transition-colors hover:bg-blue-500 hover:text-white"
        href="/"
      >
        Return to Home
      </Link>
    </div>
  );
}

import { type Fixture } from "@prisma/client";
import FlagComponent from "~/app/components/FlagComponent";
import { db } from "~/server/db";
import "/node_modules/flag-icons/css/flag-icons.min.css";
import userPrediction from "./actions";
import ScoreInput from "~/app/components/ScoreInput";
import { getServerAuthSession } from "~/server/auth";
import SaveButton from "~/app/components/SaveButton";
import Link from "next/link";

const getFixtureData = async (id: number) => {
  return await db.fixture.findFirst({
    where: { id: id },
  });
};

export default async function Fixture({ params }: { params: { id: string } }) {
  const session = await getServerAuthSession();
  const fixture = await getFixtureData(Number(params.id));

  if (!fixture) {
    throw new Error("Fixture not found");
  }

  const userId = String(session?.user.id);

  const existingPrediction = await db.userPrediction.findUnique({
    where: {
      userId_fixtureId: {
        userId: userId,
        fixtureId: fixture.id,
      },
    },
  });

  const updateWithFixture = userPrediction.bind(null, fixture);

  return (
    <>
      <form action={updateWithFixture}>
        <div className="mt-6">
          <Link className="text-xl" href={"/fixtures"}>
            &lt;
          </Link>
        </div>
        <div className="ml-5 mt-10 flex w-full flex-col gap-2 space-y-7">
          <div className="ml-10 flex gap-5">
            <FlagComponent country={fixture.homeTeam} size="small" />
            <div className=" pt-2">{fixture.homeTeam.toUpperCase()}</div>
          </div>
          <div className="flex">
            <ScoreInput
              inputName="homeTeamPrediction"
              defaultValue={
                existingPrediction
                  ? Number(existingPrediction.homeTeamPrediction)
                  : 0
              }
            />
          </div>
          <div className="ml-10 flex gap-5">
            <FlagComponent country={fixture.awayTeam} size="small" />
            <div className="pt-2">{fixture.awayTeam.toUpperCase()}</div>
          </div>
          <div className="flex ">
            <ScoreInput
              inputName="awayTeamPrediction"
              defaultValue={
                existingPrediction
                  ? Number(existingPrediction.awayTeamPrediction)
                  : 0
              }
            />
          </div>
        </div>
        <div className="flex flex-col">
          <div className="pt-8">
            <SaveButton />
          </div>
        </div>
      </form>
    </>
  );
}

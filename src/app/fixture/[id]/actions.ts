"use server";

import { type Fixture } from "@prisma/client";
import { redirect } from "next/navigation";
import { getServerAuthSession } from "~/server/auth";
import { db } from "~/server/db";
import moment from "moment-timezone";

export default async function userPrediction(
  fixture: Fixture,
  formData: FormData,
) {
  return processUserPrediction(fixture, formData);
}

const processUserPrediction = async (fixture: Fixture, formData: FormData) => {
  const userSession = await getServerAuthSession();

  const homeTeamPrediction = Number(
    formData.get("homeTeamPrediction") as string,
  );
  const awayTeamPrediction = Number(
    formData.get("awayTeamPrediction") as string,
  );

  if (!validateSubmissionTime(fixture.date, fixture.time)) {
    console.error(
      "Unable to submit prediction as the game has already started",
    );
    return redirect(`/fixture/${fixture.id}/match-started-error`);
  }

  if (!userSession) {
    throw new Error("You must be logged in to do this");
  }

  if (fixture.time) {
    const existingPrediction = await db.userPrediction.findUnique({
      where: {
        userId_fixtureId: {
          userId: userSession.user.id,
          fixtureId: fixture.id,
        },
      },
    });
    if (existingPrediction) {
      await db.userPrediction.update({
        where: {
          id: existingPrediction.id,
        },
        data: {
          id: existingPrediction.id,
          userId: userSession.user.id,
          homeTeamPrediction: homeTeamPrediction,
          awayTeamPrediction: awayTeamPrediction,
          fixtureId: existingPrediction.fixtureId,
          date: new Date(),
        },
      });
      console.log("User Prediction Updated for User: ", userSession.user.name);
    } else {
      await db.userPrediction.create({
        data: {
          userId: userSession.user.id,
          homeTeamPrediction: homeTeamPrediction,
          awayTeamPrediction: awayTeamPrediction,
          fixtureId: fixture.id,
          date: new Date(),
        },
      });
      console.log("User Prediction Added for User: ", userSession.user.name);
    }
  }

  redirect("/fixtures?saved=true");
};

const validateSubmissionTime = function (
  fixtureDate: Date,
  fixtureTimeString: string,
): boolean {
  const timeComponents: string[] = fixtureTimeString.split(":");

  if (timeComponents.length === 2) {
    const fixtureHourStr: string | undefined = timeComponents[0];
    const fixtureMinuteStr: string | undefined = timeComponents[1];

    if (fixtureHourStr && fixtureMinuteStr) {
      const londonNow = moment.tz("Europe/London");
      const currentTime = londonNow.toDate();

      const fixtureDateLondon = moment.tz(fixtureDate, "Europe/London");
      return fixtureDateLondon.isAfter(currentTime);
    } else {
      throw new Error("Invalid fixture time format: expected 'HH:MM'");
    }
  } else {
    throw new Error("Invalid fixture time format: expected 'HH:MM'");
  }
};

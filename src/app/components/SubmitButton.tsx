"use client";

import { useState } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function SubmitButton({
  isDisabled,
  successMessage,
}: {
  isDisabled?: boolean;
  successMessage?: string;
}) {
  const [isClicked, setIsClicked] = useState(false);

  const alert = () => {
    if (!isClicked && !isDisabled) {
      setIsClicked(true);
      toast.success(successMessage ?? "Successfully submitted final score", {
        position: "top-left",
      });
      setTimeout(() => {
        setIsClicked(false);
      }, 3000);
    }
  };
  return (
    <button
      className={`mt-4 w-80 rounded-lg bg-gradient-to-b from-[#242424] to-[#242424] p-5 ${isDisabled ? "text-green-600 shadow-md" : "text-white shadow-md"}`}
      type="submit"
      // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
      disabled={isDisabled}
      onClick={alert}
    >
      <div className="flex justify-center gap-3">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke={isDisabled ? "green" : "white"}
          className="h-6 w-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
          />
        </svg>
        {isDisabled ? "Submitted" : "Submit"}
      </div>
    </button>
  );
}

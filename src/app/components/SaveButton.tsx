"use client";

import { useState } from "react";
import "react-toastify/dist/ReactToastify.css";
import Spinner from "./Spinner";

export default function SaveButton() {
  const [isClicked, setIsClicked] = useState(false);

  const alert = () => {
    setIsClicked(true);
  };
  return (
    <>
      <button
        className="mt-4 w-80  rounded-lg bg-gradient-to-b from-[#242424] to-[#242424] p-5 text-white shadow-md"
        type="submit"
        onClick={alert}
      >
        {isClicked ? <Spinner text="Saving" /> : "Save"}
      </button>
    </>
  );
}

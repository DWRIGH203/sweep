"use client";

import SignIn from "./SignIn";
import Image from "next/image";
import euro2024Logo from "/src/styles/euro-2024-logo-white.jpg";
import { useState } from "react";
import { type Session } from "next-auth";
import predictionsLogo from "./football-white.png";

export default function Navbar({
  session,
  isAdmin,
}: {
  session: Session | null;
  isAdmin: boolean;
}) {
  const [isNavOpen, setIsNavOpen] = useState(false);

  return (
    <div className="flex items-center justify-between border-b border-gray-400 py-8">
      <nav>
        <section className="MOBILE-MENU flex justify-center lg:hidden">
          <a href="/">
            <Image src={euro2024Logo} alt="EURO 2024" width={75} height={75} />
          </a>

          <div
            className="HAMBURGER-ICON ml-60 space-y-2 pt-8"
            onClick={() => setIsNavOpen((prev) => !prev)}
          >
            <span className="block h-0.5 w-8 bg-white"></span>
            <span className="block h-0.5 w-8 bg-white"></span>
            <span className="block h-0.5 w-8 bg-white"></span>
          </div>

          <div className={isNavOpen ? "showMenuNav" : "hideMenuNav "}>
            <div
              className="absolute right-0 top-0 px-8 py-8"
              onClick={() => setIsNavOpen(false)}
            >
              <svg
                className="h-8 w-8 text-white"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <line x1="18" y1="6" x2="6" y2="18" />
                <line x1="6" y1="6" x2="18" y2="18" />
              </svg>
            </div>
            <ul className="flex min-h-[250px] flex-col items-center justify-between">
              <li>
                <a href="/">
                  <Image
                    src={euro2024Logo}
                    alt="EURO 2024"
                    width={75}
                    height={75}
                  />
                </a>
              </li>
              <li className="my-8 border-b border-gray-400 uppercase">
                <a href="/">Home</a>
              </li>
              {session && (
                <li className="my-8 border-b border-gray-400 uppercase">
                  <a href="/fixtures">Fixtures</a>
                </li>
              )}
              {session && (
                <li className="my-8 border-b border-gray-400 uppercase">
                  <a href={`/prediction-summary`}>Predictions</a>
                </li>
              )}
              {session && (
                <li className="my-8 border-b border-gray-400 uppercase">
                  <a href={`/selections/${session.user.id}`}>Stats</a>
                </li>
              )}
              {isAdmin && (
                <li className="my-8 border-b border-gray-400 uppercase">
                  <a href="/admin">Admin</a>
                </li>
              )}
              <li className="my-8 border-b border-gray-400 uppercase">
                <SignIn session={session} />
              </li>
            </ul>
          </div>
        </section>

        <ul className="DESKTOP-MENU hidden space-x-40 lg:flex">
          <li>
            <a href="/">
              <Image
                src={euro2024Logo}
                alt="EURO 2024"
                width={75}
                height={75}
              />
            </a>
          </li>
          <li className="pt-8">
            <a href="/">Home</a>
          </li>
          {session && (
            <li className="pt-8">
              <a href="/fixtures">Fixtures</a>
            </li>
          )}
          {session && (
            <li className="pt-8">
              <a href={`/prediction-summary`}>Predictions</a>
            </li>
          )}
          {session && (
            <li className="pt-8">
              <a href={`/selections/${session.user.id}`}>Stats</a>
            </li>
          )}
          {isAdmin && (
            <li className="pt-8">
              <a href="/admin">Admin</a>
            </li>
          )}
          <li className="pt-6">
            <SignIn session={session} />
          </li>
        </ul>
      </nav>

      <style>{`
      .hideMenuNav {
        display: none;
      }
      .showMenuNav {
        display: block;
        position: fixed;
        width: 100%;
        height: 100vh;
        top: 0;
        left: 0;
        background: #143cdb;
        z-index: 10;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        align-items: center;
      }
    `}</style>
    </div>
  );
}

"use client";

import { useEffect } from "react";
import { useSearchParams } from "next/navigation";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function SuccessBanner() {
  const searchParams = useSearchParams();
  const toastId = "saved-toast";

  useEffect(() => {
    if (searchParams.get("saved") === "true" && !toast.isActive(toastId)) {
      toast.success("Successfully saved score", {
        position: "top-left",
        toastId: toastId,
      });
      const params = new URLSearchParams(window.location.search);
      params.delete("saved");
      const newUrl = `${window.location.pathname}?${params.toString()}`;
      window.history.replaceState({}, "", newUrl);
    }
  }, [searchParams]);

  return null;
}

"use client";

import { type Session } from "next-auth";
import { signIn, signOut } from "next-auth/react";
import Image from "next/image";

export default function SignIn({ session }: { session: Session | null }) {
  return (
    <button
      onClick={async () => {
        if (session) {
          await signOut({
            callbackUrl: "/",
          });
        } else {
          await signIn("google", {
            callbackUrl: "/redirect-after-login",
          });
        }
      }}
      className="text-white-200 flex gap-2 rounded-lg border-2 border-slate-200 px-4 py-2 transition duration-150 hover:border-slate-400  hover:shadow"
    >
      <div className="w-50 flex gap-4">
        <Image
          src="https://www.svgrepo.com/show/475656/google-color.svg"
          alt="google logo"
          width={20}
          height={20}
        />
        <div className="pt-1 text-xs">
          {session ? `Sign Out` : "Login with Google"}
        </div>
      </div>
    </button>
  );
}

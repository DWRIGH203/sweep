"use client";

import Highcharts from "highcharts";
import PieChart from "highcharts-react-official";
import { useEffect, useState } from "react";
import Spinner from "./Spinner";

interface PieChartProps {
  points: number;
  winPercentage: number;
  lossPercentage: number;
}

export default function PieChartComponent({
  points,
  winPercentage,
  lossPercentage,
}: PieChartProps) {
  const [loading, setLoading] = useState(true);

  const defaultOptions: Highcharts.Options = {
    chart: {
      plotBorderWidth: 0,
      polar: true,
      backgroundColor: undefined,
      height: "350px",
      spacingTop: 1,
    },
    credits: {
      enabled: false,
    },
    title: {
      text: "<br>Total Points<br>",
      align: "center",
      verticalAlign: "middle",
      y: 105,
      x: 100,
      style: {
        fontSize: "0.9em",
        color: "white",
      },
    },
    subtitle: {
      text: points.toString(),
      align: "center",
      verticalAlign: "middle",
      y: 135,
      x: 100,
      style: {
        fontSize: "1.0em",
        color: "white",
      },
    },
    tooltip: {
      pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
    },
    accessibility: {
      enabled: false,
      point: {
        valueSuffix: "%",
      },
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: true,
          distance: -35,
          style: {
            fontWeight: "bold",
            color: "white",
          },
        },
        showInLegend: true,
        startAngle: -90,
        endAngle: 90,
        center: ["50%", "65%"],
        size: "90%",
      },
    },
    series: [
      {
        dataLabels: {
          enabled: true,
          format: "{y} %",
        },
        type: "pie",
        innerSize: "50%",
        data: [
          ["Win", winPercentage != 0 ? winPercentage : null],
          ["Loss", lossPercentage != 0 ? lossPercentage : null],
        ],
      },
    ],
    legend: {
      enabled: true,
      align: "center",
      verticalAlign: "middle",
      layout: "horizontal",
      y: 105,
      x: -90,
      itemStyle: {
        color: "white",
      },
    },
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      setLoading(false);
    }, 100);

    return () => clearTimeout(timeout);
  }, []);

  return (
    <>
      {loading ? (
        <div className="mb-6 mt-6">
          <Spinner />
        </div>
      ) : (
        <PieChart highcharts={Highcharts} options={defaultOptions} />
      )}
    </>
  );
}

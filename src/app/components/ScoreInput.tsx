"use client";

import { useState } from "react";

export default function ScoreInput({
  inputName,
  defaultValue,
}: {
  inputName: string;
  defaultValue?: number;
}) {
  const [value, setValue] = useState(defaultValue ?? 0);

  return (
    <div className="flex flex-wrap justify-center">
      <button
        className="rounded-l-full bg-gray-200 px-3 py-1 text-gray-700"
        onClick={() => setValue(Math.max(0, value - 1))}
        type="button"
      >
        -
      </button>

      <input
        className="border px-5 py-3 text-center font-semibold text-black no-underline transition [appearance:textfield] hover:border-gray-400 [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none"
        type="number"
        id={inputName}
        name={inputName}
        required
        value={value}
        readOnly
      />
      <button
        className="rounded-r-full bg-gray-200 px-3 py-1 text-gray-700"
        onClick={() => setValue(value + 1)}
        type="button"
      >
        +
      </button>
    </div>
  );
}

import React from "react";
import FlagComponent from "./FlagComponent";
import "/node_modules/flag-icons/css/flag-icons.min.css";
import { type Fixture } from "@prisma/client";

export default function FixtureCard({
  fixture,
  userHasPrediction,
}: {
  fixture: Fixture;
  userHasPrediction?: boolean;
}) {
  return (
    <div className="flex-container rounded-lg border-slate-200 bg-gradient-to-b from-[#0d1e62a5] from-5% via-[#0d1e62a5] via-40% to-[#0d1e62a5] to-80% font-sans">
      <div
        className="bg-gray mt-6 flex flex-col rounded-lg p-4 shadow-md"
        style={{ width: "22rem" }}
      >
        <div className="text-blue-gray-700 mb-5 flex justify-between font-bold">
          <span>{fixture.time}</span>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={3}
            stroke={userHasPrediction ? "green" : "white"}
            className="h-6 w-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
            />
          </svg>
        </div>
        <div
          className="mb-2 flex items-center justify-between"
          style={{ marginLeft: "-15px" }}
        >
          <div className="flex w-1/2 items-center justify-end space-x-4">
            <span className="w-24 truncate text-right">{fixture.homeTeam}</span>
            <FlagComponent country={fixture.homeTeam} size="small" />
          </div>
          <div className="mx-4 flex w-1/3 items-center justify-center">vs</div>
          <div className="flex w-1/2 items-center justify-start space-x-4">
            <FlagComponent country={fixture.awayTeam} size="small" />
            <span className="w-24 truncate text-left">{fixture.awayTeam}</span>
          </div>
        </div>
      </div>
    </div>
  );
}

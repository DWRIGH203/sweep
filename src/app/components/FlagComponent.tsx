import "/node_modules/flag-icons/css/flag-icons.min.css";

type FlagProps = {
  country: string;
  size: string;
};

const countryCodes: Record<string, string> = {
  SCOTLAND: "gb-sct",
  ENGLAND: "gb-eng",
  GERMANY: "de",
  SPAIN: "es",
  FRANCE: "fr",
  ITALY: "it",
  HUNGARY: "hu",
  SWITZERLAND: "ch",
  CROATIA: "hr",
  AUSTRIA: "at",
  ALBANIA: "al",
  BELGIUM: "be",
  POLAND: "pl",
  PORTUGAL: "pt",
  ROMANIA: "ro",
  DENMARK: "dk",
  SLOVENIA: "si",
  SERBIA: "rs",
  UKRAINE: "ua",
  SLOVAKIA: "sk",
  TÜRKIYE: "tr",
  CZECHIA: "cz",
  GEORGIA: "ge",
  NETHERLANDS: "nl",
};

export default function FlagComponent({ country, size }: FlagProps) {
  const countryCode = countryCodes[country.toUpperCase()];

  if (size === "small") {
    return <div className={`fib fi-${countryCode} h-10 w-10`}></div>;
  }
}

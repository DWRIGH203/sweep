"use client";

import Link from "next/link";
import React, { useState } from "react";
import FixtureCard from "./FixtureCard";
import Spinner from "./Spinner"; // Adjust the import path as necessary
import { type FixtureWithPrediction } from "~/types";

type GroupedFixturesProps = {
  groupedFixtures: [string, FixtureWithPrediction[]][];
};

const GroupedFixtures: React.FC<GroupedFixturesProps> = ({
  groupedFixtures,
}) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const handlePrevDate = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex > 0 ? prevIndex - 1 : groupedFixtures.length - 1,
    );
  };

  const handleNextDate = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex < groupedFixtures.length - 1 ? prevIndex + 1 : 0,
    );
  };

  const handleLinkClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    setIsLoading(true);
    const href = e.currentTarget.href;
    setTimeout(() => {
      window.location.href = href;
    }, 300);
  };

  const currentFixtures = groupedFixtures?.[currentIndex]?.[1];
  const currentDate = groupedFixtures?.[currentIndex]?.[0];

  return (
    <div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          margin: "20px 0",
        }}
      >
        <button onClick={handlePrevDate}>&lt;</button>
        <p style={{ margin: "0 20px", fontWeight: "bold" }}>{currentDate}</p>
        <button onClick={handleNextDate}>&gt;</button>
      </div>
      <div className="flex flex-col items-center space-y-4">
        {isLoading ? (
          <div className="mt-8">
            <Spinner />
          </div>
        ) : (
          currentFixtures?.map((fixtureWithPrediction, index) => (
            <div key={index}>
              <Link
                href={`/fixture/${fixtureWithPrediction.fixture.id}`}
                onClick={handleLinkClick}
              >
                <FixtureCard
                  key={index}
                  fixture={fixtureWithPrediction.fixture}
                  userHasPrediction={fixtureWithPrediction.userHasPrediction}
                />
              </Link>
            </div>
          ))
        )}
      </div>
    </div>
  );
};

export default GroupedFixtures;

import "~/styles/globals.css";
import { Inter } from "next/font/google";
import Navbar from "./components/Navbar";
import { getServerAuthSession } from "~/server/auth";
import { isAdmin } from "./utils/utils";
import { ToastContainer } from "react-toastify";
import type { Metadata, Viewport } from "next";

const inter = Inter({
  subsets: ["latin"],
  variable: "--font-sans",
});

const APP_NAME = "Sweep";
const APP_DEFAULT_TITLE = "Euro 2024";
const APP_TITLE_TEMPLATE = "%s - EURO 2024";
const APP_DESCRIPTION = "Score predictor competition for Euro 2024";

export const metadata: Metadata = {
  icons: [{ rel: "icon", url: "/favicon.ico" }],
  applicationName: APP_NAME,
  title: {
    default: APP_DEFAULT_TITLE,
    template: APP_TITLE_TEMPLATE,
  },
  description: APP_DESCRIPTION,
  manifest: "/manifest.json",
  appleWebApp: {
    capable: true,
    statusBarStyle: "default",
    title: APP_DEFAULT_TITLE,
    // startUpImage: [],
  },
  formatDetection: {
    telephone: false,
  },
  openGraph: {
    type: "website",
    siteName: APP_NAME,
    title: {
      default: APP_DEFAULT_TITLE,
      template: APP_TITLE_TEMPLATE,
    },
    description: APP_DESCRIPTION,
  },
  twitter: {
    card: "summary",
    title: {
      default: APP_DEFAULT_TITLE,
      template: APP_TITLE_TEMPLATE,
    },
    description: APP_DESCRIPTION,
  },
};

export const viewport: Viewport = {
  themeColor: "#143cdb",
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const session = await getServerAuthSession();
  const isAdminRole = await isAdmin(session);
  return (
    <html lang="en">
      <body
        className={`font-sans ${inter.variable} bg-gradient-to-b from-[#143cdb] from-20%  via-[#143cdb] via-100% to-[#143cdb]`}
      >
        <main className="">
          <div className="scrollbar-hide flex min-h-screen flex-col items-center justify-start overflow-hidden">
            <div className="container flex flex-col items-center justify-center px-2 py-4 text-white">
              <Navbar session={session} isAdmin={isAdminRole} />
              <ToastContainer></ToastContainer>
              {children}
            </div>
          </div>
        </main>
      </body>
    </html>
  );
}
